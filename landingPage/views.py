from django.shortcuts import render, redirect
from .forms import StatusForm
from .models import Status

def index(request):
	statusModel = Status.objects.all()
	statusForm = StatusForm(request.POST or None)
	if request.method == 'POST':
		if statusForm.is_valid():
			statusForm.save()
			return redirect('/')

	args = {
		'status' : statusModel,
		'form'	 : statusForm,
	}

	return render(request, 'index.html', args)
