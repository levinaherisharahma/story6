from django.test import LiveServerTestCase, TestCase, Client
from django.urls import resolve
from .models import Status
from .forms import StatusForm
from .views import *

class Test(TestCase):
	def test_ada_url_kosong(self):
		c = Client()
		response = c.get('//')
		self.assertEqual(response.status_code, 200)

	def test_ada_tulisan_halo_apa_kabar(self):
		c = Client()
		response = c.get('//')
		content = response.content.decode('utf8')
		self.assertIn("Halo, apa kabar?", content)

	def test_landingPage_menggunakan_index_html(self):
		c = Client()
		response = c.get('//')
		self.assertTemplateUsed(response, 'index.html')

	def test_landingPage_pakai_fungsi_index(self):
		function = resolve('/')
		self.assertEqual(function.func, index)

	def test_apakah_ada_form(self):
		c = Client()
		response = c.get('//')
		content = response.content.decode('utf8')
		self.assertIn("<form", content)

	def test_ada_button_kirim(self):
		c = Client()
		response = c.get('//')
		content = response.content.decode('utf8')
		self.assertIn("<button", content)
		self.assertIn("Kirim", content)

	def test_ada_table_isi_status(self):
		c = Client()
		response = c.get('//')
		content = response.content.decode('utf8')
		self.assertIn("<table", content)
		self.assertIn("Waktu", content)
		self.assertIn("Status", content)

	def test_bisa_buat_status_baru(self):
		new_status = Status.objects.create(status='This is fine :)')
		count = Status.objects.all().count()
		self.assertEqual(count, 1)

	def test_form_validation_for_blank_items(self):
		form = StatusForm(data={'status': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['status'], ["This field is required."])


from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class Story6FunctionalTest(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Story6FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Story6FunctionalTest, self).tearDown()

	def test_input_status(self):
		selenium = self.selenium
		# buka link yg mau dites
		selenium.get(self.live_server_url)

		# cari elemen form
		status = selenium.find_element_by_id('id_status')
		submit = selenium.find_element_by_id('submit')

		# isi form
		status.send_keys('Coba Coba')

		# kirim form
		submit.send_keys(Keys.RETURN)
